WOM2Vec Module Repository
=========================

WOM2Vec is a package for analyzing political data made publicly available via
`Wahl-O-Mat <http://www.bpb.de/politik/wahlen/wahl-o-mat/>`_. It generates
a neural word embedding of manifestos using gensim [1]_ and processes them
using an approach proposed by Tulkens et al. [2]_. These processed topic
vectors are then used for SVM classification using scikit-learn's SVC
implementation [3]_, in order to predict a party's viewpoint based on training
examples.

.. [1] http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.695.4595
.. [2] https://arxiv.org/abs/1608.05605
.. [3] http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html
