# -*- coding: utf-8 -*-
"""WOM2Vec example file.
Copyright (c) 2017, Nils Patrick Müller

This module contains usage examples for the WOM2Vec package.
"""

import wom2vec.builder
import wom2vec.classifier
import wom2vec.plotter
import wom2vec.vectools

def main():
    source = "./data/txt/"

    # Create corpus based on NRW Landtagswahl 2017 manifestos
    corpus = wom2vec.builder.create_corpus(source + "manifesto/")

    # Create dictionary with party answers based on Wahl-O-Mat data for NRW Landtagswahl 2017
    answers = wom2vec.builder.create_answer_dictionary(source + "antworten.txt")

    # Create dictionary with theses based on Wahl-O-Mat data for NRW Landtagswahl 2017
    theses = wom2vec.builder.create_thesis_dictionary(source + "thesen.txt")

    # Create dictionary with trends based on Wahl-O-Mat data for NRW Landtagswahl 2017
    trends = wom2vec.builder.create_trends_dictionary(source + "trends.txt")

    # Compute neural word embedding based on corpus using word2vec implementation of gensim
    model = wom2vec.builder.compute_model(corpus)

    parteien_im_landtag = ["AFD", "CDU", "FDP", "GRUENE", "SPD"]
    # antworten_spd = wom2vec.vectools.compute_party_answers(answers["SPD"], model)
    parteien_embeddings = wom2vec.vectools.compute_party_vectors(answers, model, parteien_im_landtag)
    # antworten_these_5 = wom2vec.vectools.compute_thesis_answers(answers, model, parteien_im_landtag, 5)

    # Predict answers and compute precision of predicted answers
    for party in parteien_im_landtag:
        party_answers = wom2vec.vectools.compute_party_answers(answers[party], model)
        party_trends = trends[party]
        training_size = int(0.9 * len(party_trends))

        party_score = wom2vec.classifier.predict_party_answers(party_answers, party_trends, training_size)
        print("{party} score: {score}".format(party=party, score=party_score))

    # wom2vec.plotter.plot_dendrogram(parteien_embeddings)
    # wom2vec.plotter.plot_dendrogram(antworten_these_5, theses[5])

if __name__ == "__main__":
    main()
