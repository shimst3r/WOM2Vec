# -*- coding: utf-8 -*-

import context

from wom2vec import *
import sklearn.decomposition
import matplotlib.pyplot
import numpy

def main():
    source = "../data/txt/"

    corpus = builder.create_corpus(source + "manifesto/")

    answers = builder.create_answer_dictionary(source + "antworten.txt")

    trends = builder.create_trends_dictionary(source + "trends.txt")

    model = builder.compute_model(corpus)

    sublist = ["AFD", "CDU", "FDP", "GRUENE", "SPD"]

    for party in sublist:
        p_answer = vectools.compute_party_answers(answers[party], model)
        p_trends =  trends[party]

        pca = sklearn.decomposition.PCA(n_components=2)
        X = pca.fit(p_answer).transform(p_answer)

        new = list(map(lambda x,y: numpy.append(x,y), X, p_trends))

        vec_p = [vec for vec in new if vec[-1] == 1.]
        vec_n = [vec for vec in new if vec[-1] == -1.]
        vec_0 = [vec for vec in new if vec[-1] == 0.]

        print("{p}\t{n}\t{z}".format(p=len(vec_p), n=len(vec_n), z=len(vec_0)))

        matplotlib.pyplot.figure()

        if vec_p:
            matplotlib.pyplot.scatter(map(lambda x: x[0], vec_p),map(lambda x: x[1], vec_p), color="r")
        if vec_n:
            matplotlib.pyplot.scatter(map(lambda x: x[0], vec_n),map(lambda x: x[1], vec_n), color="b")
        if vec_0:
            matplotlib.pyplot.scatter(map(lambda x: x[0], vec_0),map(lambda x: x[1], vec_0), color="g")

        matplotlib.pyplot.title(party)
        matplotlib.pyplot.xlabel("x distance")
        matplotlib.pyplot.ylabel("y distance")
        matplotlib.pyplot.show()

if __name__ == "__main__":
    main()
