# -*- coding: utf-8 -*-

import context

from wom2vec import *
import sklearn.decomposition
import matplotlib.pyplot
import numpy

def main():
    source = "../data/txt/"

    corpus = builder.create_corpus(source + "manifesto/")

    answers = builder.create_answer_dictionary(source + "antworten.txt")

    trends = builder.create_trends_dictionary(source + "trends.txt")

    model = builder.compute_model(corpus)

    sublist = ["AFD", "CDU", "FDP", "GRUENE", "SPD"]

    for party in sublist:
        p_answer = vectools.compute_party_answers(answers[party], model)
        p_trends =  trends[party]
        training_s = int(0.9 * len(p_trends))

        pca = sklearn.decomposition.PCA(n_components=2)
        X = pca.fit(p_answer).transform(p_answer)

        p_score = classifier.predict_party_answers(X, p_trends, training_s)

        print("{party} classifier precision: {score}".format(party=party, score=p_score))

if __name__ == "__main__":
    main()
