# -*- coding: utf-8 -*-
"""WOM2Vec vectools module.
Copyright (c) 2017, Nils Patrick Müller

This module contains helper functions for operations on word embedding vectors.
"""

import gensim
import nltk
import numpy

def compute_party_answers(answers, model):
    """Compute averaged embedding vectors for each answer for a given party.

    Parameters
    ----------
    answers : list
        A list of answers.

    Returns
    -------
    list
        A list of averaged embedding vectors
    """

    # Replace words with corresponding embedding vector
    vectorized_answers = [[model.wv[word] for word in answer if word in model.wv.vocab and word not in nltk.corpus.stopwords.words("german")] for answer in answers]

    # Average each vectorized answer -- empty answers are replaced with zero vectors
    answer_vectors = [sum([vector for vector in vectors])/len(vectors) if len(vectors) != 0 else numpy.zeros(model.vector_size) for vectors in vectorized_answers]
    return answer_vectors

def compute_thesis_answers(dictionary, model, sublist, thesis):
    """Compute averaged embedding vectors for a specific thesis for all parties in dictionary.

    Parameters
    ----------
    dictionary : dict
        A dictionary of the form {party : [answers]}.
    model : gensim.models.word2vec.Word2Vec
        A neural word embedding model.
    sublist : list
        A list restricting the considered parties.
    thesis : int
        The index of considered thesis (1-based!).

    Returns
    -------
    dict
        A dict of the form {party : averaged_party_vector}.
    """

    if sublist:
        parties = sublist
    else:
        parties = dictionary.keys()

    # Iterate over all parties in dictionary keys and compute average of one of their answer embeddings
    dictionary = {party : compute_party_answers(dictionary[party], model)[thesis - 1] for party in parties}

    return dictionary

def compute_party_vectors(dictionary, model, sublist):
    """Compute averaged party vectors for all parties in dictionary.

    Parameters
    ----------
    dictionary : dict
        A dictionary of the form {party : [answers]}.
    model : gensim.models.word2vec.Word2Vec
        A neural word embedding model.
    sublist : list
        A list restricting the considered parties.

    Returns
    -------
    dict
        A dict of the form {party : averaged_party_vector}.
    """

    if sublist:
        parties = sublist
    else:
        parties = dictionary.keys()

    # Iterate over all parties in dictionary keys and compute average of all of their answer embeddings
    dictionary = {party : sum(compute_party_answers(dictionary[party], model))/len(compute_party_answers(dictionary[party], model)) for party in parties}

    return dictionary
