# -*- coding: utf-8 -*-
"""WOM2Vec builder module.
Copyright (c) 2017, Nils Patrick Müller

This module is responsible for creating a corpus based on manifesto files (typically available as text
files) and computing a word2Vec model using gensim based on this corpus, as well as building thesis and answer dictionaries.
"""

import codecs
import gensim
import nltk
import numpy
import os

def create_corpus(source_directory):
    """Create a corpus from all manifesto files in the given directory.

    Parameters
    ----------
    source_directory : str
        The path to a directory containing manifesto text files.

    Returns
    -------
    list
        A list of sentences.
    """

    # Initialize raw corpus list
    raw_corpus = []

    # Iterate over all lines of all files in source directory and append lines to raw_corpus
    for filename in os.listdir(source_directory):
        with codecs.open(source_directory + filename, 'r', 'UTF-8') as input_file:
            for line in input_file.readlines():
                raw_corpus.append(line)

    # Join raw lines of text and tokenize into sentences using NLTK sentence tokenizer
    sentence_tokens = nltk.sent_tokenize("".join(raw_corpus))

    # Split sentence tokens into word2Vec training format (lists of words) using NLTK word tokenizer
    final_corpus = [[word for word in nltk.word_tokenize(sentence) if word.isalpha()] for sentence in sentence_tokens]

    return final_corpus

def create_answer_dictionary(source_path):
    """Create a dictionary based on answers to Wahl-O-Mat theses.

    Parameters
    ----------
    source_path : str
        The path to a file containing Wahl-O-Mat answers.

    Returns
    -------
    dict
        A dictionary of the form {party : {index : answer}}.
    """

    # Open and read Wahl-O-Mat data file
    with codecs.open(source_path, "r", "utf-8") as input_file:
        raw_answers = input_file.readlines()

    # Initialize dictionary
    dictionary = {}

    # Iterate over all sentences in Wahl-O-Mat data and add them to corresponing party
    for line in raw_answers:
        word_tokens = nltk.word_tokenize(line)

        if(word_tokens):  # check for non-empty sentence
            party = word_tokens[0]
            answer = word_tokens[1:]

            if party in dictionary.keys():  # check if party is already in dictionary
                dictionary[party].append(answer)
            else:
                dictionary[party] = [answer]

    return dictionary

def create_thesis_dictionary(source_path):
    """Create a dictionary of Wahl-O-Mat theses.

    Parameters
    ----------
    source_path : str
        The path to a file containing Wahl-O-Mat theses.

    Returns
    -------
    dict
        A dictionary of the form {thesis_index : thesis}.
    """

    # Iterate over all lines in Wahl-O-Mat thesis file. Each line represents a thesis.
    with codecs.open(source_path, "r", "UTF-8") as input_file:
        dictionary = {int(line.split()[0]) : " ".join(line.split()[1:]) for line in input_file.readlines()}

    return dictionary

def create_trends_dictionary(source_path):
    """Create a dictionary of Wahl-O-Mat trends, ie positive, negative oder neutral reaction to theses.

    Parameters
    ----------
    source_path : str
        The path to a file containing Wahl-O-Mat trends.

    Returns
    -------
    dict
        A dictionar of the form {party : numpy.asarray([trends])}.
    """

    # Initialize dictionary
    dictionary = {}

    # Iterate over all lines in Wahl-O-Mat trends file and add value to corresponding party.
    with codecs.open(source_path, "r", "UTF-8") as input_file:
        for line in input_file.readlines():
            party = line.split()[1]
            value = int(line.split()[0])

            if party in dictionary.keys():
                dictionary[party].append(value)
            else:
                dictionary[party] = [value]

        for party in dictionary.keys():
            dictionary[party] = numpy.asarray(dictionary[party])

    return dictionary

def compute_model(corpus):
    """Compute a word2Vec model based on a previously created corpus.

    Parameters
    ----------
    corpus : list
        A list of sentences where sentences are split into words with NLTK word tokenizer

    Returns
    -------
    gensim.models.word2vec.Word2Vec
        A word2Vec model based on corpus
    """

    # A word2Vec model with dimensionality 800 and window size 5, using CBOW and negative sampling
    model = gensim.models.word2vec.Word2Vec(corpus, size=400, window=5, sg=0, hs=0)

    return model
