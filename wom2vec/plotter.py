# -*- coding: utf-8 -*-
"""WOM2Vec plotter module.
Copyright (c) 2017, Nils Patrick Müller

This module contains plotting functions.
"""

import matplotlib.pyplot
import scipy.cluster.hierarchy
import sklearn.decomposition

title = "Hierarchical clustering of German political parties based on neural word embeddings"

def plot_dendrogram(features, title=title):
    """Compute 2-dimensional PCA of feature vectors and plot corresponding dendrogram.

    Parameters
    ----------
    features : dict
        A dict of the form {party : averaged_party_vector}.
    index : int

    Returns
    -------
    bool
    """

    parties = features.keys()
    vectors = features.values()

    pca = sklearn.decomposition.PCA(n_components=2)
    X = pca.fit(vectors).transform(vectors)
    Z = scipy.cluster.hierarchy.linkage(X, "ward")

    matplotlib.pyplot.figure()
    matplotlib.pyplot.title(title)
    matplotlib.pyplot.ylabel("euclidian distance after 2-dimensional PCA")

    scipy.cluster.hierarchy.dendrogram(Z, labels=parties, leaf_rotation=45)

    matplotlib.pyplot.show()
