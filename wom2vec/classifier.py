# -*- coding: utf-8 -*-
"""WOM2Vec classifier module.
Copyright (c) 2017, Nils Patrick Müller

This module is responsible for analyzing Wahl-O-Mat embeddings using SVM techniques.
"""

import sklearn.multiclass
import sklearn.svm

def predict_party_answers(answers, trends, training_size):
    """Predict a party's possible answers and check them against real answers.

    Parameters
    ----------
    answers : list
        A list of neural word embeddings representing answers.
    trends : list
        A list of positive, negativ or neutral trends, represented as {+1, -1, 0}.
    test_size : int
        Number of training vectors.

    Returns
    -------
    numpy.float64
        Classification score.
    """

    # Using 30 answer embeddings and trend values for training
    training_embeddings = answers[:training_size]
    training_trends = trends[:training_size]

    # Using 8 answer embeddings and trend values for testing
    testing_embeddings = answers[training_size:]
    testing_trends = trends[training_size:]

    base_classifier = sklearn.svm.SVC(kernel="sigmoid")
    classifier = sklearn.multiclass.OneVsRestClassifier(base_classifier).fit(training_embeddings, training_trends)

    return classifier.score(testing_embeddings, testing_trends)
